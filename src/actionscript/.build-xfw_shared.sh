#!/bin/bash

# XVM team (c) https://modxvm.com 2014-2019
# XFW Framework build system

set -e

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh

detect_os
detect_actionscript_sdk

class="com.xfw.Xfw"
build_as3_swc \
    -inline \
    -source-path xfw_shared \
    -external-library-path+=../../~output/swc/wg_shared.swc \
    -output ../../~output/swc/xfw_shared.swc \
    -include-classes $class
