#!/bin/bash

# XVM team (c) https://modxvm.com 2014-2019
# XFW Framework build system

set -e

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh

detect_os
detect_actionscript_sdk

class="com.xfw.XfwView"
build_as3_swc \
    -inline \
    -source-path xfw \
    -external-library-path+=../../~output/swc/wg_lobby.swc \
    -include-libraries+=../../~output/swc/xfw_shared.swc \
    -output ../../~output/swc/xfw.swc \
    -include-classes $class

doc="xfw/com/xfw/XfwView.as"
build_as3_swf \
    -inline \
    -source-path xfw \
    -external-library-path+=../../~output/swc/wg_lobby.swc \
    -include-libraries+=../../~output/swc/xfw_shared.swc \
    -output ../../~output/swf/xfw.swf \
    $doc
