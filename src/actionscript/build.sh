#!/bin/bash

# XVM team (c) https://modxvm.com 2014-2019
# XFW Framework build system

set -e

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh
PATH="$currentdir"/../../build/bin/msil/:$PATH

mkdir -p "$currentdir/../../~output/swf/"

projects="
    wg_shared
    wg_lobby
    wg_battle
    wg_battle_classic_ui
    wg_battle_epicbattle_ui
    wg_battle_epicrandom_ui
    wg_battle_ranked_ui
    wg_vm
    xfw_shared
    xfw
"

for project in ${projects}; do
    echo "building ${project}"
    . .build-${project}.sh
done

echo ""
