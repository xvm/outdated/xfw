#!/bin/bash

# XVM team (c) https://modxvm.com 2014-2019
# XFW Framework build system

set -e

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh

detect_os
detect_actionscript_sdk

class="App"
build_as3_swc \
    -source-path wg/lobby \
    -source-path wg/lobby_links \
    -source-path wg/lobby_ui/* \
    -source-path wg/common_i18n \
    -output ../../~output/swc/wg_lobby.swc \
    -include-classes $class
