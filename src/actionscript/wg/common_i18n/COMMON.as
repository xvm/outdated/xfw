package
{
    public class COMMON extends Object
    {

        public static const COMMON_COLON:String = "#common:common/colon";

        public static const COMMON_PERCENT:String = "#common:common/percent";

        public static const COMMON_DASH:String = "#common:common/dash";

        public static const COMMON_SLASH:String = "#common:common/slash";

        public static const COMMON_OPEN_QUOTES:String = "#common:common/open_quotes";

        public static const COMMON_CLOSE_QUOTES:String = "#common:common/close_quotes";

        public static const COMMON_DOT:String = "#common:common/dot";

        public static const CLANTAG:String = "#common:clanTag";

        public function COMMON()
        {
            super();
        }
    }
}
