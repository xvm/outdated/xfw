package
{
    public class DEVELOPMENT extends Object
    {

        public static const BOTS_TITLE:String = "#development:bots/title";

        public static const BOTS_LABELS_VEHICLECLASS:String = "#development:bots/labels/vehicleClass";

        public static const BOTS_LABELS_TEAMONE:String = "#development:bots/labels/teamOne";

        public static const BOTS_LABELS_TEAMTWO:String = "#development:bots/labels/teamTwo";

        public static const BOTS_BUTTONS_ADD:String = "#development:bots/buttons/add";

        public static const BOTS_BUTTONS_CLOSE:String = "#development:bots/buttons/close";

        public static const MESSENGER_CONTACTS_SEARCHCONTACT_HEADER:String = "#development:messenger/contacts/searchContact/header";

        public static const MESSENGER_CONTACTS_SEARCHCONTACT_BODY:String = "#development:messenger/contacts/searchContact/body";

        public static const MESSENGER_CONTACTS_ADDGROUP_HEADER:String = "#development:messenger/contacts/addGroup/header";

        public static const MESSENGER_CONTACTS_ADDGROUP_BODY:String = "#development:messenger/contacts/addGroup/body";

        public static const MESSENGER_CONTACTS_SETTINGS_HEADER:String = "#development:messenger/contacts/settings/header";

        public static const MESSENGER_CONTACTS_SETTINGS_BODY:String = "#development:messenger/contacts/settings/body";

        public static const MESSENGER_CONTACTS_SETTINGS_VIEW_APPLYBTN_HEADER:String = "#development:messenger/contacts/settings/view/applyBtn/header";

        public static const MESSENGER_CONTACTS_SETTINGS_VIEW_APPLYBTN_BODY:String = "#development:messenger/contacts/settings/view/applyBtn/body";

        public static const MESSENGER_CONTACTS_SETTINGS_VIEW_CANCELBTN_HEADER:String = "#development:messenger/contacts/settings/view/cancelBtn/header";

        public static const MESSENGER_CONTACTS_SETTINGS_VIEW_CANCELBTN_BODY:String = "#development:messenger/contacts/settings/view/cancelBtn/body";

        public static const MESSENGER_CONTACTS_EXTERNALSEARCH_HEADER:String = "#development:messenger/contacts/externalSearch/header";

        public static const MESSENGER_CONTACTS_EXTERNALSEARCH_BODY:String = "#development:messenger/contacts/externalSearch/body";

        public static const EDITOR_CONTEXTMENU_NEW:String = "#development:editor/contextMenu/new";

        public static const EDITOR_CONTEXTMENU_OPEN:String = "#development:editor/contextMenu/open";

        public static const EDITOR_CONTEXTMENU_SAVE:String = "#development:editor/contextMenu/save";

        public static const EDITOR_CONTEXTMENU_SAVE_AS:String = "#development:editor/contextMenu/save_as";

        public static const EDITOR_CONTEXTMENU_UNDO:String = "#development:editor/contextMenu/undo";

        public static const EDITOR_CONTEXTMENU_CUT:String = "#development:editor/contextMenu/cut";

        public static const EDITOR_CONTEXTMENU_PASTE:String = "#development:editor/contextMenu/paste";

        public static const EDITOR_CONTEXTMENU_COPY:String = "#development:editor/contextMenu/copy";

        public static const EDITOR_CONTEXTMENU_CLOSE_EDITOR:String = "#development:editor/contextMenu/close_editor";

        public static const EDITOR_GOTO:String = "#development:editor/goto";

        public static const EDITOR_SEARCH:String = "#development:editor/search";

        public static const EDITOR_PAUSE:String = "#development:editor/Pause";

        public static const EDITOR_RESUME:String = "#development:editor/Resume";

        public static const WULF_CONTENT_PAGER:String = "#development:wulf/content/pager";

        public static const WULF_PROPS_TITLE:String = "#development:wulf/props/title";

        public static const WULF_UNBOUNDINJECTIONWINDOW_TITLE:String = "#development:wulf/unboundInjectionWindow/title";

        public static const WULF_UIKITWINDOW_HEADER:String = "#development:wulf/uiKitWindow/header";

        public static const WULF_UIKITWINDOW_PRIMARYBTN:String = "#development:wulf/uiKitWindow/PrimaryBtn";

        public static const WULF_UIKITWINDOW_PROGRESSBARSMALL:String = "#development:wulf/uiKitWindow/ProgressBarSmall";

        public static const WULF_UIKITWINDOW_PROGRESSBARBIG:String = "#development:wulf/uiKitWindow/ProgressBarBig";

        public static const WULF_UIKITWINDOW_INPUT:String = "#development:wulf/uiKitWindow/Input";

        public static const WULF_UIKITWINDOW_LIST:String = "#development:wulf/uiKitWindow/List";

        public static const WULF_UIKITWINDOW_SLOTBTN:String = "#development:wulf/uiKitWindow/SlotBtn";

        public static const WULF_UIKITWINDOW_SLIDER:String = "#development:wulf/uiKitWindow/Slider";

        public static const WULF_UIKITWINDOW_CHECKBOX:String = "#development:wulf/uiKitWindow/CheckBox";

        public static const WULF_UIKITWINDOW_CHECKBOXLABEL:String = "#development:wulf/uiKitWindow/CheckBoxLabel";

        public static const WULF_WTYPESDEMO_TOOLTIP_HEADER:String = "#development:wulf/wTypesDemo/tooltip/header";

        public static const WULF_WTYPESDEMO_TOOLTIP_BODY:String = "#development:wulf/wTypesDemo/tooltip/body";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENWINDOWS:String = "#development:wulf/wTypesDemo/contextMenu/openWindows";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENSTANDARDWINDOW:String = "#development:wulf/wTypesDemo/contextMenu/openStandardWindow";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENSERVICEWINDOW:String = "#development:wulf/wTypesDemo/contextMenu/openServiceWindow";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENINFODIALOG:String = "#development:wulf/wTypesDemo/contextMenu/openInfoDialog";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENCONFIRMDIALOG:String = "#development:wulf/wTypesDemo/contextMenu/openConfirmDialog";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENOVERLAY:String = "#development:wulf/wTypesDemo/contextMenu/openOverlay";

        public static const WULF_DEMOWINDOW_TITLE:String = "#development:wulf/demoWindow/title";

        public static const WULF_LISTEXAMPLESWINDOW_TITLE:String = "#development:wulf/listExamplesWindow/title";

        public static const WULF_LISTEXAMPLESEMPTYRENDERWINDOW_TITLE:String = "#development:wulf/listExamplesEmptyRenderWindow/title";

        public static const WULF_SERVICEWINDOWDEMO_TITLE:String = "#development:wulf/serviceWindowDemo/title";

        public static const WULF_STANDARTWINDOWDEMO_TITLE:String = "#development:wulf/standartWindowDemo/title";

        public static const WULF_WINDOWTYPESDEMO_TITLE:String = "#development:wulf/windowTypesDemo/title";

        public static const WULF_WTYPESDEMO_CONTEXTMENU_OPENDIALOGS:String = "#development:wulf/wTypesDemo/contextMenu/openDialogs";

        public static const WULF_BUTTONS_BUY:String = "#development:wulf/buttons/buy";

        public static const WULF_BUTTONS_CONTACTS:String = "#development:wulf/buttons/contacts";

        public static const WULF_BUTTONS_CANCEL:String = "#development:wulf/buttons/cancel";

        public static const WULF_BUTTONS_TOGGLE:String = "#development:wulf/buttons/toggle";

        public static const WULF_LISTEXAMPLESWINDOW_RENDERERLABEL:String = "#development:wulf/listExamplesWindow/rendererLabel";

        public static const WULF_GFDEMOWINDOW_TITLE:String = "#development:wulf/gfDemoWindow/title";

        public static const WULF_GFDEMOWINDOW_BTNPREV_LABEL:String = "#development:wulf/gfDemoWindow/btnPrev/label";

        public static const WULF_GFDEMOWINDOW_BTNNEXT_LABEL:String = "#development:wulf/gfDemoWindow/btnNext/label";

        public static const WULF_DIALOGEXAMPLES_TESTARGS_TITLE:String = "#development:wulf/dialogExamples/testArgs/title";

        public static const WULF_DIALOGEXAMPLES_TESTARGS_MESSAGE:String = "#development:wulf/dialogExamples/testArgs/message";

        public static const WULF_DIALOGEXAMPLES_TESTARGS_SUBMIT:String = "#development:wulf/dialogExamples/testArgs/submit";

        public static const WULF_DIALOGEXAMPLES_TESTARGS_CANCEL:String = "#development:wulf/dialogExamples/testArgs/cancel";

        public static const WULF_DIALOGEXAMPLES_TESTFMTARGS_TITLE:String = "#development:wulf/dialogExamples/testFmtArgs/title";

        public static const WULF_DIALOGEXAMPLES_TESTFMTARGS_MESSAGE:String = "#development:wulf/dialogExamples/testFmtArgs/message";

        public static const WULF_DIALOGEXAMPLES_TESTFMTARGS_SUBMIT:String = "#development:wulf/dialogExamples/testFmtArgs/submit";

        public static const WULF_DIALOGEXAMPLES_TESTFMTARGS_CANCEL:String = "#development:wulf/dialogExamples/testFmtArgs/cancel";

        public function DEVELOPMENT()
        {
            super();
        }
    }
}
