package
{
    public class PREBATTLE extends Object
    {

        public static const LABELS_PLAYER:String = "#prebattle:labels/player";

        public static const LABELS_VEHICLE:String = "#prebattle:labels/vehicle";

        public static const LABELS_LEVEL:String = "#prebattle:labels/level";

        public static const LABELS_MEMBERS:String = "#prebattle:labels/members";

        public static const LABELS_STATS_MAXPLAYERS:String = "#prebattle:labels/stats/maxPlayers";

        public static const LABELS_STATS_TOTALLEVEL:String = "#prebattle:labels/stats/totalLevel";

        public static const LABELS_STATS_SUMLEVEL:String = "#prebattle:labels/stats/sumLevel";

        public static const LABELS_STATS_LEVEL:String = "#prebattle:labels/stats/level";

        public static const LABELS_STATS_VEHICLELEVEL:String = "#prebattle:labels/stats/vehicleLevel";

        public static const LABELS_STATS_VEHICLESPGLEVEL:String = "#prebattle:labels/stats/vehicleSPGLevel";

        public static const DIALOGS_BUTTONS_READY:String = "#prebattle:dialogs/buttons/ready";

        public static const DIALOGS_BUTTONS_NOTREADY:String = "#prebattle:dialogs/buttons/notReady";

        public static const TITLE_BATTLESESSION_STARTTIME:String = "#prebattle:title/battleSession/startTime";

        public static const TITLE_BATTLESESSION_HEADER_STARTTIME:String = "#prebattle:title/battleSession/header/startTime";

        public static const TITLE_BATTLESESSION_COMMENT:String = "#prebattle:title/battleSession/comment";

        public static const TITLE_BATTLESESSION_ARENATYPE:String = "#prebattle:title/battleSession/arenaType";

        public static const TITLE_BATTLESESSION_BATTLESLIMIT:String = "#prebattle:title/battleSession/battlesLimit";

        public static const STATS_BATTLESESSION_COMMONLEVEL:String = "#prebattle:stats/battleSession/commonLevel";

        public static const STATS_BATTLESESSION_REQUIRED:String = "#prebattle:stats/battleSession/required";

        public static const STATS_BATTLESESSION_VEHICLETYPE:String = "#prebattle:stats/battleSession/vehicleType";

        public static const BUTTONS_BATTLESESSION_LEAVE:String = "#prebattle:buttons/battleSession/leave";

        public static const LABELS_COMPANY_QUEUE:String = "#prebattle:labels/company/queue";

        public static const SWITCHPERIPHERYWINDOW_FORT_HEADER:String = "#prebattle:switchPeripheryWindow/fort/header";

        public static const SWITCHPERIPHERYWINDOW_FORT_DESCRIPTION:String = "#prebattle:switchPeripheryWindow/fort/description";

        public static const SWITCHPERIPHERYWINDOW_FORT_SELECTSERVERLABEL:String = "#prebattle:switchPeripheryWindow/fort/selectServerLabel";

        public static const SWITCHPERIPHERYWINDOW_FORT_APPLYSWITCHLABEL:String = "#prebattle:switchPeripheryWindow/fort/applySwitchLabel";

        public static const SWITCHPERIPHERYWINDOW_RANKED_HEADER:String = "#prebattle:switchPeripheryWindow/ranked/header";

        public static const SWITCHPERIPHERYWINDOW_RANKED_DESCRIPTION:String = "#prebattle:switchPeripheryWindow/ranked/description";

        public static const SWITCHPERIPHERYWINDOW_RANKED_SELECTSERVERLABEL:String = "#prebattle:switchPeripheryWindow/ranked/selectServerLabel";

        public static const SWITCHPERIPHERYWINDOW_RANKED_APPLYSWITCHLABEL:String = "#prebattle:switchPeripheryWindow/ranked/applySwitchLabel";

        public static const COMPANYLISTVIEW_FILTERBUTTON_LABEL:String = "#prebattle:companyListView/filterButton/label";

        public static const MEMBERLIST_EMPTY_LABEL:String = "#prebattle:memberList/empty/label";

        public static const BATTLEPROGRESS_HINT:String = "#prebattle:battleProgress/hint";

        public static const UPBUTTON_HINT:String = "#prebattle:upButton/hint";

        public function PREBATTLE()
        {
            super();
        }
    }
}
