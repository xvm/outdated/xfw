package net.wg.gui.lobby.vehiclePreview20.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class VPCustomOfferVO extends DAAPIDataClass
    {

        public var name:String = "";

        public var value:String = "";

        public function VPCustomOfferVO(param1:Object = null)
        {
            super(param1);
        }
    }
}
