package net.wg.gui.lobby.modulesPanel.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IUpdatable;
    import net.wg.infrastructure.interfaces.IPopOverCaller;

    public interface IDeviceSlot extends IUpdatable, IPopOverCaller
    {
    }
}
