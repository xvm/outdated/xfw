package net.wg.gui.lobby.vehicleCustomization
{
    import flash.display.Sprite;
    import net.wg.gui.interfaces.IContentSize;
    import net.wg.infrastructure.interfaces.entity.IDisposable;
    import net.wg.gui.components.controls.SimpleTileList;
    import flash.text.TextField;
    import net.wg.gui.lobby.vehicleCustomization.data.styleInfo.StyleInfoVO;
    import scaleform.clik.data.DataProvider;
    import scaleform.clik.constants.DirectionMode;
    import flash.text.TextFieldAutoSize;

    public class CustomizationStyleScrollContainer extends Sprite implements IContentSize, IDisposable
    {

        private static const PARAM_RENDERERS_GAP:int = 46;

        private static const PARAM_RENDERERS_GAP_SMALL:int = 20;

        private static const PARAM_RENDERED_WIDTH:int = 110;

        private static const STYLE_INFO_TEXT_OFFSET_Y:int = 36;

        private static const PARAM_RENDERER_NAME:String = "ParamIconRendererUI";

        public var paramsBlock:SimpleTileList;

        public var styleInfoTf:TextField = null;

        public var suitableTf:TextField = null;

        private var _styleInfoText:String = "";

        private var _styleInfoTextBig:String = "";

        public function CustomizationStyleScrollContainer()
        {
            super();
            this.paramsBlock.itemRenderer = App.utils.classFactory.getClass(PARAM_RENDERER_NAME);
            this.paramsBlock.directionMode = DirectionMode.HORIZONTAL;
            this.paramsBlock.tileWidth = PARAM_RENDERED_WIDTH;
            this.styleInfoTf.autoSize = this.suitableTf.autoSize = TextFieldAutoSize.LEFT;
        }

        public function get contentHeight() : Number
        {
            return this.suitableTf.y + this.suitableTf.height | 0;
        }

        public function get contentWidth() : Number
        {
            return width;
        }

        public function dispose() : void
        {
            if(this.paramsBlock)
            {
                this.paramsBlock.dispose();
                this.paramsBlock = null;
            }
            this.styleInfoTf = null;
            this.suitableTf = null;
            this._styleInfoText = null;
            this._styleInfoTextBig = null;
        }

        public function updateSize(param1:Boolean, param2:Boolean) : void
        {
            this.paramsBlock.horizontalGap = param2?PARAM_RENDERERS_GAP_SMALL:PARAM_RENDERERS_GAP;
            this.paramsBlock.border.width = this.styleInfoTf.width = this.suitableTf.width = this.width;
            this.styleInfoTf.htmlText = param1 || param2?this._styleInfoText:this._styleInfoTextBig;
            var _loc3_:* = 0;
            var _loc4_:StyleInfoRenderer = null;
            var _loc5_:int = this.paramsBlock.length;
            var _loc6_:* = 0;
            while(_loc6_ < _loc5_)
            {
                _loc4_ = StyleInfoRenderer(this.paramsBlock.getRendererAt(_loc6_));
                _loc3_ = Math.max(_loc3_,_loc4_.contentHeight);
                _loc6_++;
            }
            this.styleInfoTf.y = _loc3_ + STYLE_INFO_TEXT_OFFSET_Y;
            this.suitableTf.y = this.styleInfoTf.y + this.styleInfoTf.height;
        }

        public function setData(param1:StyleInfoVO) : void
        {
            this._styleInfoText = param1.styleInfo;
            this._styleInfoTextBig = param1.styleInfoBig;
            this.suitableTf.htmlText = param1.suitableBlock;
            this.paramsBlock.dataProvider = new DataProvider(App.utils.data.vectorToArray(param1.styleParams));
        }
    }
}
