package net.wg.gui.lobby.vehicleCustomization.controls.slotsGroup
{
    public interface ICustomizationSlot
    {

        function isWide() : Boolean;
    }
}
