package net.wg.gui.lobby.tankman.vo
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class PersonalCaseTabNameVO extends DAAPIDataClass
    {

        public var index:uint = 1.0;

        public var label:String = "";

        public var linkage:String = "";

        public function PersonalCaseTabNameVO(param1:Object)
        {
            super(param1);
        }
    }
}
