package net.wg.gui.lobby.vehiclePreview20.data
{
    public class VPPageVO extends VPPageBaseVO
    {

        public var vehicleTitle:String = "";

        public var vehicleName:String = "";

        public var vehicleTypeIcon:String = "";

        public var nationFlagIcon:String = "";

        public var nationName:String = "";

        public var compareBtnTooltip:String = "";

        public var showCompareBtn:Boolean;

        public var listDesc:String = "";

        public var isMultinational:Boolean;

        public function VPPageVO(param1:Object)
        {
            super(param1);
        }
    }
}
