package net.wg.gui.lobby.rankedBattles19.components.divisionStatus
{
    import net.wg.gui.components.common.FrameStateCmpnt;
    import flash.display.BlendMode;

    public class DivisionStatus extends FrameStateCmpnt
    {

        public function DivisionStatus()
        {
            super();
            blendMode = BlendMode.SCREEN;
        }
    }
}
