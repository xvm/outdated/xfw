package net.wg.gui.lobby.modulesPanel.data
{
    public class BattleAbilityVO extends DeviceVO
    {

        public var desc:String = "";

        public var level:int = -1;

        public var changeOrderButtonLabel:String = "";

        public var filterText:String = "";

        public function BattleAbilityVO(param1:Object)
        {
            super(param1);
        }
    }
}
