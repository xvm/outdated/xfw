package net.wg.gui.bootcamp.constants
{
    public class BOOTCAMP_DISPLAY extends Object
    {

        public static const HANGAR_VEH_RESEARCH_PANEL:String = "HangarVehResearchPanel";

        public static const HANGAR_TMEN_XP_PANEL:String = "HangarTmenXpPanel";

        public static const HANGAR_PARAMS:String = "HangarParams";

        public static const HANGAR_AMMUNITION_PANEL:String = "HangarAmmunitionPanel";

        public static const HANGAR_CREW_OPERATIONS:String = "HangarCrewOperations";

        public static const HANGAR_CAROUSEL:String = "HangarCarousel";

        public static const HANGAR_SWITCH_MODE_PANEL:String = "HangarSwitchModePanel";

        public static const HANGAR_QUEST_CONTROL:String = "HangarQuestControl";

        public static const HANGAR_CREW:String = "HangarCrew";

        public static const HANGAR_BOTTOM_BACKGROUND:String = "HangarBottomBackground";

        public static const HANGAR_HEADER:String = "HangarHeader";

        public static const HEADER:String = "Header";

        public static const HANGAR_VEHICLE_STATE:String = "HangarVehicleState";

        public static const HANGAR_MAINTENANCE_BUTTONS:String = "HangarMaintenanceButtons";

        public static const HANGAR_COMPONENTS:String = "HangarComponents";

        public static const HANGAR_OPTIONAL_DEVICES:String = "HangarOptionalDevices";

        public static const HANGAR_SHELLS:String = "HangarShells";

        public static const HANGAR_EQUIPMENT:String = "HangarEquipment";

        public static const HEADER_MAIN_MENU_BUTTON_BAR:String = "HeaderMainMenuButtonBar";

        public function BOOTCAMP_DISPLAY()
        {
            super();
        }
    }
}
