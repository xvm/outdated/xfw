package net.wg.gui.login.impl.vo
{
    public class SubmitDataVo extends Object
    {

        public var login:String = "";

        public var pwd:String = "";

        public var host:String = "";

        public var isSocial:Boolean = false;

        public function SubmitDataVo()
        {
            super();
        }
    }
}
