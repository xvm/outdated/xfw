package net.wg.gui.components.interfaces
{
    import scaleform.clik.interfaces.IListItemRenderer;
    import net.wg.infrastructure.interfaces.IReusable;

    public interface IReusableListItemRenderer extends IListItemRenderer, IReusable
    {
    }
}
