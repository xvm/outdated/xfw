package net.wg.gui.prebattle.battleSession
{
    import net.wg.infrastructure.base.UIComponentEx;
    import flash.text.TextField;

    public class TopInfo extends UIComponentEx
    {

        public var firstTeamText:TextField;

        public var winTeamsText:TextField;

        public var secondTeamText:TextField;

        public var startTimeText:TextField;

        public var startTimeValue:TextField;

        public function TopInfo()
        {
            super();
        }
    }
}
