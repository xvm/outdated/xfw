package net.wg.gui.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class CrystalsPromoWindowVO extends DAAPIDataClass
    {

        public var windowTitle:String = "";

        public var headerTF:String = "";

        public var subTitle0:String = "";

        public var subDescr0:String = "";

        public var subTitle1:String = "";

        public var subDescr1:String = "";

        public var subTitle2:String = "";

        public var subDescr2:String = "";

        public var closeBtn:String = "";

        public var openShopBtnLabel:String = "";

        public var image0:String = "";

        public var image1:String = "";

        public var image2:String = "";

        public var bg:String = "";

        public var showOpenShopBtn:Boolean = false;

        public function CrystalsPromoWindowVO(param1:Object)
        {
            super(param1);
        }
    }
}
