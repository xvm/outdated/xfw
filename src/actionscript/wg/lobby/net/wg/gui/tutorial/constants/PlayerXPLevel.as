package net.wg.gui.tutorial.constants
{
    public class PlayerXPLevel extends Object
    {

        public static var NEWBIE:Number = 0;

        public static var NORMAL:Number = 1;

        public function PlayerXPLevel()
        {
            super();
        }
    }
}
