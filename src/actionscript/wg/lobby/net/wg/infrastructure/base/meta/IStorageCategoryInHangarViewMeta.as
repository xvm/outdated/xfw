package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IStorageCategoryInHangarViewMeta extends IEventDispatcher
    {

        function as_setTabsData(param1:Array) : void;
    }
}
