package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface ICrystalsPromoWindowMeta extends IEventDispatcher
    {

        function onOpenShopS() : void;

        function as_setData(param1:Object) : void;
    }
}
