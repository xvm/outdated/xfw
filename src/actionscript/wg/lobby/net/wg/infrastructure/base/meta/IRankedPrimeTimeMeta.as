package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IRankedPrimeTimeMeta extends IEventDispatcher
    {

        function as_setHeaderData(param1:Object) : void;
    }
}
