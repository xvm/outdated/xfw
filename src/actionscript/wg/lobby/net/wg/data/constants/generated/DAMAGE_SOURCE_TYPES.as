package net.wg.data.constants.generated
{
    public class DAMAGE_SOURCE_TYPES extends Object
    {

        public static const HEAVY_TANK:String = "heavyTank";

        public static const MEDIUM_TANK:String = "mediumTank";

        public static const LIGHT_TANK:String = "lightTank";

        public static const AT_SPG:String = "AT-SPG";

        public static const SPG:String = "SPG";

        public static const ARTILLERY:String = "artillery";

        public static const AIRSTRIKE:String = "airstrike";

        public static const DAMAGE_SOURCES:Array = [HEAVY_TANK,MEDIUM_TANK,LIGHT_TANK,AT_SPG,SPG,ARTILLERY,AIRSTRIKE];

        public function DAMAGE_SOURCE_TYPES()
        {
            super();
        }
    }
}
