package net.wg.data.constants.generated
{
    public class RANKEDBATTLES_CONSTS extends Object
    {

        public static const RANKED_BATTLES_RANKS_ID:String = "rankedBattlesRanks";

        public static const RANKED_BATTLES_REWARDS_ID:String = "rankedBattlesRewards";

        public static const RANKED_BATTLES_RATING_ID:String = "rankedBattlesRating";

        public static const RANKED_BATTLES_INFO_ID:String = "rankedBattlesInfo";

        public static const RANKED_BATTLES_REWARDS_RANKEDS_ID:String = "rankedBattlesRewardsRankeds";

        public static const RANKED_BATTLES_REWARDS_LEAGUES_ID:String = "rankedBattlesRewardsLeagues";

        public static const RANKED_BATTLES_REWARDS_YEAR_ID:String = "rankedBattlesRewardsYear";

        public static const RANKED_REWARDS_RANK_RECEIVED:String = "received";

        public static const RANKED_REWARDS_RANK_CURRENT:String = "current";

        public static const RANKED_REWARDS_RANK_NEXT:String = "next";

        public static const RANKED_REWARDS_RANK_LOCKED:String = "locked";

        public static const RANKED_REWARDS_REWARD_SIZE_SMALL:String = "small";

        public static const RANKED_REWARDS_REWARD_SIZE_BIG:String = "big";

        public static const RANKED_REWARDS_REWARDS_COUNT_4:int = 4;

        public static const RANKED_REWARDS_REWARDS_COUNT_5:int = 5;

        public static const RANKED_REWARDS_REWARDS_COUNT_7:int = 7;

        public static const STATUS_ID_DONE:String = "done";

        public static const STATUS_ID_LOCK:String = "lock";

        public static const STATUS_ID_CURRENT:String = "current";

        public static const RANKED_REWARDS_YEAR_SMALL:String = "small";

        public static const RANKED_REWARDS_YEAR_MEDIUM:String = "medium";

        public static const RANKED_REWARDS_YEAR_BIG:String = "big";

        public static const RANKED_REWARDS_YEAR_LARGE:String = "large";

        public static const RANKED_REWARDS_YEAR_MAIN_AVAILABLE_FOR:Array = [RANKED_REWARDS_YEAR_BIG,RANKED_REWARDS_YEAR_LARGE];

        public static const YEAR_REWARD_STATUS_PASSED:String = "passed";

        public static const YEAR_REWARD_STATUS_LOCKED:String = "locked";

        public static const YEAR_REWARD_STATUS_CURRENT:String = "current";

        public function RANKEDBATTLES_CONSTS()
        {
            super();
        }
    }
}
