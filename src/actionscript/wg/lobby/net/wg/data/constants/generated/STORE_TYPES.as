package net.wg.data.constants.generated
{
    public class STORE_TYPES extends Object
    {

        public static const SHOP:String = "shop";

        public static const INVENTORY:String = "inventory";

        public function STORE_TYPES()
        {
            super();
        }
    }
}
