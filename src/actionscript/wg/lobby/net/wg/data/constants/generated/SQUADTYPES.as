package net.wg.data.constants.generated
{
    public class SQUADTYPES extends Object
    {

        public static const SQUAD_TYPE_SIMPLE:String = "squadTypeSimple";

        public static const SIMPLE_SQUAD_TEAM_SECTION:String = "SimpleSquadTeamSection_UI";

        public static const SIMPLE_SQUAD_CHAT_SECTION:String = "SimpleSquadChatSection_UI";

        public function SQUADTYPES()
        {
            super();
        }
    }
}
