package net.wg.data.constants
{
    public class DelayTypes extends Object
    {

        public static const GLOBAL:String = "global";

        public static const LOCAL:String = "local";

        public function DelayTypes()
        {
            super();
        }
    }
}
