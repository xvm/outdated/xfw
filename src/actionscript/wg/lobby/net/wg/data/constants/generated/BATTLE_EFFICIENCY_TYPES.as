package net.wg.data.constants.generated
{
    public class BATTLE_EFFICIENCY_TYPES extends Object
    {

        public static const ARMOR:String = "armor";

        public static const DAMAGE:String = "damage";

        public static const RAM:String = "ram";

        public static const BURN:String = "burn";

        public static const DESTRUCTION:String = "kill";

        public static const TEAM_DESTRUCTION:String = "teamKill";

        public static const DETECTION:String = "spotted";

        public static const ASSIST_TRACK:String = "assistTrack";

        public static const ASSIST_SPOT:String = "assistSpot";

        public static const CRITS:String = "crits";

        public static const CAPTURE:String = "capture";

        public static const DEFENCE:String = "defence";

        public static const ASSIST:String = "assist";

        public static const WORLD_COLLISION:String = "worldCollision";

        public static const RECEIVED_CRITS:String = "receivedCrits";

        public static const RECEIVED_DAMAGE:String = "receivedDamage";

        public static const RECEIVED_BURN:String = "receivedBurn";

        public static const RECEIVED_RAM:String = "receivedRam";

        public static const RECEIVED_WORLD_COLLISION:String = "receivedWorldCollision";

        public static const ASSIST_STUN:String = "assistStun";

        public static const STUN:String = "stun";

        public static const VEHICLE_RECOVERY:String = "vehicleRecovery";

        public static const ENEMY_SECTOR_CAPTURED:String = "enemySectorCaptured";

        public static const DESTRUCTIBLE_DAMAGED:String = "destructibleDamaged";

        public static const DESTRUCTIBLE_DESTROYED:String = "destructibleDestroyed";

        public static const DESTRUCTIBLES_DEFENDED:String = "destructiblesDefended";

        public static const DEFENDER_BONUS:String = "defenderBonus";

        public static const BASE_CAPTURE_BLOCKED:String = "baseCaptureBlocked";

        public static const ASSIST_BY_ABILITY:String = "assistByAbility";

        public function BATTLE_EFFICIENCY_TYPES()
        {
            super();
        }
    }
}
