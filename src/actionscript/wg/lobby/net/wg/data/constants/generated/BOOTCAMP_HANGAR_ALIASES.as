package net.wg.data.constants.generated
{
    public class BOOTCAMP_HANGAR_ALIASES extends Object
    {

        public static const RESEARCH_PANEL:String = "bootcampResearchPanel";

        public static const AMMUNITION_PANEL:String = "bootcampAmmunitionPanel";

        public static const TANK_CAROUSEL:String = "bootcampTankCarousel";

        public static const CREW:String = "bootcampCrew";

        public static const HEADER:String = "bootcampHeader";

        public function BOOTCAMP_HANGAR_ALIASES()
        {
            super();
        }
    }
}
