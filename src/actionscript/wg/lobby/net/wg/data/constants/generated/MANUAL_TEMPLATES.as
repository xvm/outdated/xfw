package net.wg.data.constants.generated
{
    public class MANUAL_TEMPLATES extends Object
    {

        public static const HINTS:String = "ManualPageDetailedHintsUI";

        public static const BOOTCAMP:String = "ManualPageDetailedBootcampUI";

        public function MANUAL_TEMPLATES()
        {
            super();
        }
    }
}
