package net.wg.data.constants.generated
{
    public class TUTORIAL_EFFECT_TYPES extends Object
    {

        public static const HINT:String = "hint";

        public static const BOOTCAMP_HINT:String = "bootcampHint";

        public static const DISPLAY:String = "display";

        public static const TWEEN:String = "tween";

        public static const CLIP:String = "clip";

        public static const ENABLED:String = "enabled";

        public static const OVERLAY:String = "overlay";

        public static const LAYOUT:String = "layout";

        public function TUTORIAL_EFFECT_TYPES()
        {
            super();
        }
    }
}
