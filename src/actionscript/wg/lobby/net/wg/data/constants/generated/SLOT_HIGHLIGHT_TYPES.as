package net.wg.data.constants.generated
{
    public class SLOT_HIGHLIGHT_TYPES extends Object
    {

        public static const OPTIONAL_DEVICE_NAME:String = "optionalDevice";

        public static const BATTLE_BOOSTER_NAME:String = "battleBooster";

        public static const BATTLE_BOOSTER:String = "battleBooster";

        public static const BATTLE_BOOSTER_BIG:String = "battleBoosterBig";

        public static const BATTLE_BOOSTER_CREW_REPLACE:String = "battleBoosterReplace";

        public static const BATTLE_BOOSTER_CREW_REPLACE_BIG:String = "battleBoosterReplaceBig";

        public static const EQUIPMENT_PLUS:String = "equipmentPlus";

        public static const EQUIPMENT_PLUS_BIG:String = "equipmentPlusBig";

        public static const BUILT_IN_EQUIPMENT:String = "builtIn";

        public static const BUILT_IN_EQUIPMENT_BIG:String = "builtInBig";

        public static const NO_HIGHLIGHT:String = "";

        public static const TOOLTIP_OVERLAY_PADDING_LEFT:int = -25;

        public static const TOOLTIP_OVERLAY_PADDING_TOP:int = -23;

        public static const TOOLTIP_HIGHLIGHT_PADDING_LEFT:int = -18;

        public static const TOOLTIP_HIGHLIGHT_PADDING_TOP:int = -18;

        public function SLOT_HIGHLIGHT_TYPES()
        {
            super();
        }
    }
}
