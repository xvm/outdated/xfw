package net.wg.data.constants.generated
{
    public class BOOTCAMP_MESSAGE_ALIASES extends Object
    {

        public static const RENDERER_FIN_UI:String = "BCMessageFinUI";

        public static const RENDERER_ORANGE_UI:String = "BCMessageOrangeUI";

        public static const RENDERER_BLUE:String = "BCMessageBlueUI";

        public static const RENDERER_GOLD:String = "BCMessageGoldUI";

        public static const RENDERER_INTRO:String = "BCMessageIntroUI";

        public static const BOTTOM_REWARDS_VIEW_UI:String = "BottomRewardsViewUI";

        public static const BOTTOM_NODES_VIEW_UI:String = "BottomNodesViewUI";

        public static const BOTTOM_BUTTONS_TANK_VIEW_UI:String = "BottomButtonsTankViewUI";

        public static const BOTTOM_BUTTONS_VIEW_UI:String = "BottomButtonsViewUI";

        public static const BOTTOM_NODE_LINE_VIEW_UI:String = "BottomNodeLineViewUI";

        public function BOOTCAMP_MESSAGE_ALIASES()
        {
            super();
        }
    }
}
