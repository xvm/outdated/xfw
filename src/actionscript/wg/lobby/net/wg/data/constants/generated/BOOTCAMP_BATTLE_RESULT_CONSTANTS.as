package net.wg.data.constants.generated
{
    public class BOOTCAMP_BATTLE_RESULT_CONSTANTS extends Object
    {

        public static const MEDAlS_LIST:String = "medalsList";

        public static const STATS_LIST:String = "statsList";

        public function BOOTCAMP_BATTLE_RESULT_CONSTANTS()
        {
            super();
        }
    }
}
