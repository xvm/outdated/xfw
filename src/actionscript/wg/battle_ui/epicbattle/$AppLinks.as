package
{

internal class $AppLinks
{

/**
 *  @private
 *  This class is used to link additional classes into wg_*.swc
 *  beyond those that are found by dependecy analysis starting
 *  from the classes specified in manifest.xml.
 */

import net.wg.gui.battle.epicBattle.battleloading.EpicBattleLoadingForm; EpicBattleLoadingForm;
import net.wg.gui.battle.epicBattle.battleloading.renderers.EpicBattleLoadingPlayerItemRenderer; EpicBattleLoadingPlayerItemRenderer;
import net.wg.gui.battle.epicBattle.views.EpicBattlePage; EpicBattlePage;

/**
 * UIs
 */

// epicBattleLoading.swf
EpicBattleLoadingUI;

// epicFullStats.swf
EpicFullStatsUI;

// teamBasesPanel.swf
teamBasesPanelUI;
TeamCaptureBarUI;

// minimap.swf
minimapUI;
ArcadeCameraEntry;
CellFlashEntry;
DeadPointEntry;
StrategicCameraEntry;
VideoCameraEntry;
ViewPointEntry;
ViewRangeCirclesEntry;
VehicleEntry;

// sixthSense.swf
sixthSenseUI;

}

}
