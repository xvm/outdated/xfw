package net.wg.gui.battle.views.minimap.components.entries.constants
{
    public class FortConsumablesMinimapEntryConst extends Object
    {

        public static const RECON_ATLAS_ITEM_NAME:String = "ReconEntry";

        public static const ARTILLERY_ATLAS_ITEM_NAME:String = "ArtilleryEntry";

        public static const BOMBER_ATLAS_ITEM_NAME:String = "BomberEntry";

        public static const SMOKE_ATLAS_ITEM_NAME:String = "SmokeEntry";

        public function FortConsumablesMinimapEntryConst()
        {
            super();
        }
    }
}
