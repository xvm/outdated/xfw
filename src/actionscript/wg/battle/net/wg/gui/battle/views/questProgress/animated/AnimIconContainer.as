package net.wg.gui.battle.views.questProgress.animated
{
    import flash.display.Sprite;
    import net.wg.infrastructure.interfaces.entity.IDisposable;
    import net.wg.gui.components.controls.UILoaderAlt;

    public class AnimIconContainer extends Sprite implements IDisposable
    {

        public var taskIcon:UILoaderAlt = null;

        public function AnimIconContainer()
        {
            super();
        }

        public final function dispose() : void
        {
            this.taskIcon.dispose();
            this.taskIcon = null;
        }

        public function set source(param1:String) : void
        {
            this.taskIcon.source = param1;
        }
    }
}
