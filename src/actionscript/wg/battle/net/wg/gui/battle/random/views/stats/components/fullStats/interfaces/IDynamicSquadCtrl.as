package net.wg.gui.battle.random.views.stats.components.fullStats.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IDisposable;
    import net.wg.gui.battle.components.buttons.interfaces.IClickButtonHandler;
    import net.wg.gui.battle.components.buttons.interfaces.IRollOutButtonHandler;

    public interface IDynamicSquadCtrl extends IDisposable, IClickButtonHandler, IRollOutButtonHandler
    {
    }
}
