package net.wg.gui.battle.views.consumablesPanel.interfaces
{
    import net.wg.infrastructure.base.meta.IConsumablesPanelMeta;
    import net.wg.gui.battle.components.buttons.interfaces.IClickButtonHandler;

    public interface IConsumablesPanel extends IConsumablesPanelMeta, IClickButtonHandler
    {
    }
}
