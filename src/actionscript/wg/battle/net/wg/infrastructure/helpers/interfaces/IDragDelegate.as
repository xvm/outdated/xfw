package net.wg.infrastructure.helpers.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IDraggable;
    import net.wg.infrastructure.interfaces.entity.IDisposable;

    public interface IDragDelegate extends IDraggable, IDisposable
    {
    }
}
