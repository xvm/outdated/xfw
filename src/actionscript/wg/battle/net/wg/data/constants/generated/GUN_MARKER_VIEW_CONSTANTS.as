package net.wg.data.constants.generated
{
    public class GUN_MARKER_VIEW_CONSTANTS extends Object
    {

        public static const GUN_MARKER_LINKAGE:String = "GunMarkerUI";

        public static const GUN_MARKER_DEBUG_LINKAGE:String = "GunMarkerDebugUI";

        public static const GUN_MARKER_SPG_LINKAGE:String = "GunMarkerSPGUI";

        public static const GUN_MARKER_SPG_DEBUG_LINKAGE:String = "GunMarkerSPGDebugUI";

        public static const ARTY_HIT_MARKER_LINKAGE:String = "ArtyHitMarkerUI";

        public static const ARCADE_GUN_MARKER_NAME:String = "arcadeGunMarker";

        public static const SNIPER_GUN_MARKER_NAME:String = "sniperGunMarker";

        public static const SPG_GUN_MARKER_NAME:String = "spgGunMarker";

        public static const VIDEO_GUN_MARKER_NAME:String = "videoGunMarker";

        public static const ARTY_HIT_MARKER_NAME:String = "artyHitGunMarker";

        public static const DEBUG_ARCADE_GUN_MARKER_NAME:String = "arcadeDebugGunMarker";

        public static const DEBUG_SNIPER_GUN_MARKER_NAME:String = "sniperGunDebugMarker";

        public static const DEBUG_SPG_GUN_MARKER_NAME:String = "spgDebugGunMarker";

        public static const GUN_TAG_RELOADING_TYPES:Array = [1,4,7,10,13];

        public static const GUN_TAG_SHOT_RESULT_TYPES:Array = [0,2,3,6,9,12];

        public static const DISPERSION_CIRCLE_RELOADING_TYPES:Array = [0,1,2];

        public function GUN_MARKER_VIEW_CONSTANTS()
        {
            super();
        }
    }
}
