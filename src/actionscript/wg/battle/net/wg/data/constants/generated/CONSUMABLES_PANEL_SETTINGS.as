package net.wg.data.constants.generated
{
    public class CONSUMABLES_PANEL_SETTINGS extends Object
    {

        public static const DEFAULT_SETTINGS_ID:int = 0;

        public static const BIG_SETTINGS_ID:int = 1;

        public static const GLOW_ID_GREEN:int = 0;

        public static const GLOW_ID_ORANGE:int = 1;

        public static const GLOW_ID_GREEN_SPECIAL:int = 2;

        public function CONSUMABLES_PANEL_SETTINGS()
        {
            super();
        }
    }
}
