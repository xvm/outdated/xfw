package net.wg.data.constants
{
    public class Locales extends Object
    {

        public static const CHINA:String = "CN";

        public static const VIETNAM:String = "VN";

        public static const KOREA:String = "KR";

        public function Locales()
        {
            super();
        }
    }
}
