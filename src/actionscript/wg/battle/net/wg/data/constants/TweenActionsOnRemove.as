package net.wg.data.constants
{
    public class TweenActionsOnRemove extends Object
    {

        public static const STOP:String = "stop";

        public static const NOT_TO_PROCESS:String = "notToProcess";

        public function TweenActionsOnRemove()
        {
            super();
        }
    }
}
