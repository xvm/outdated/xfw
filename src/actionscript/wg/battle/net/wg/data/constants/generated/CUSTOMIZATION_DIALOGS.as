package net.wg.data.constants.generated
{
    public class CUSTOMIZATION_DIALOGS extends Object
    {

        public static const CUSTOMIZATION_INSTALL_BOUND_CHECK_NOTIFICATION:String = "customization/install_bound/check";

        public static const CUSTOMIZATION_INSTALL_BOUND_BASKET_NOTIFICATION:String = "customization/install_bound/basket";

        public static const CUSTOMIZATION_INSTALL_BOUND_RIGHTCLICK_NOTIFICATION:String = "customization/install_bound/right_click";

        public function CUSTOMIZATION_DIALOGS()
        {
            super();
        }
    }
}
