package
{

internal class $AppLinks
{

/**
 *  @private
 *  This class is used to link additional classes into wg.swc
 *  beyond those that are found by dependecy analysis starting
 *  from the classes specified in manifest.xml.
 */

import net.wg.data.constants.ImageCacheTypes; ImageCacheTypes;
import net.wg.infrastructure.events.LoaderEvent; LoaderEvent;
import scaleform.gfx.TextFieldEx; TextFieldEx;

}

}
