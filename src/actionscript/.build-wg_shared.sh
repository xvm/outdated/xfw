#!/bin/bash

# XVM team (c) https://modxvm.com 2014-2019
# XFW Framework build system

set -e

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh

detect_os
detect_actionscript_sdk

class="App"
build_as3_swc \
    -source-path wg/shared \
    -output ../../~output/swc/wg_shared.swc \
    -include-classes $class
